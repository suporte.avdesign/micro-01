<?php

namespace App\Observers;

use App\Models\Category;
use Illuminate\Support\Str;

class CategoryObserver
{
    /**
     * Antes de criar
     */
    public function creating(Category $category): void
    {
        $category->url = Str::slug($category->title, '-');
    }

    /**
     * Antes de fazer o update
     */
    public function updating(Category $category): void
    {
        $category->url = Str::slug($category->title, '-');
    }
}
