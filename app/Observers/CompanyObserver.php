<?php

namespace App\Observers;

use App\Models\Company;
use Illuminate\Support\Str;

class CompanyObserver
{
    /**
     * Handle the Company "creating" event.
     */
    public function creating(Company $company): void
    {
        $company->url = Str::slug($company->name);
        $company->uuid = Str::uuid();
    }

    /**
     * Handle the Company "updating" event.
     */
    public function updating(Company $company): void
    {
        $company->url = Str::slug($company->name);
    }

    
}
